import { expect, tap } from '@pushrocks/tapbundle';
import * as webclient from '../ts/index.js';

let testClient: webclient.CsWebclient;

tap.test('should create an instance of webclient', async () => {
  testClient = new webclient.CsWebclient();
  expect(testClient).toBeInstanceOf(webclient.CsWebclient);
});

tap.test('should return false when asked for present settings', async () => {
  const result = await testClient.isCookieLevelSet();
  expect(result).toBeFalse();
});

tap.test('should create a cookielevel setting', async () => {
  await testClient.setCookieLevels(['functional']);
  const result = await testClient.getCookieLevels();
  expect(result).toContain('functional');
});

tap.test('should return true when asked for present settings', async () => {
  const result = await testClient.isCookieLevelSet();
  expect(result).toBeTrue();
});

tap.test('should validity for present settings', async () => {
  const result = await testClient.isCookieLevelStillValid();
  expect(result).toBeTrue();
});

tap.start();
