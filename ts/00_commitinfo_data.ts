/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@consentsoftware/webclient',
  version: '1.0.15',
  description: 'a webclient for using consent.software in your website. Works with vanilla js, angular, react, you name it.'
}
